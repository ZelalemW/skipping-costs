# Skipping costs #

In addition to not knowing [Learning steps](https://bitbucket.org/ZelalemW/learning-steps/src/master/README.md), often times, i notice folks skipping fundamentally required skills and focus on tools and wizard drag drops.

Well, this may be the case for some types of roles, knowing the tool well, drag dropping stuff here and there and setting some properties and boooom, you got it done. Manager and everyone around you is happy.

In some cases (especially in core development) however, that is not the way to go. Skipping fundamntals - even if you think you are not using them on day to day basis is a wrong move and will cost you a lot of headaches and frustrations. **If you don't know how the technology/code fundamentally works**

``` 
How will you know where to start debugging an issue? Trial and error? Duckduckgo?

How will you choose the different data structure or algorithm that best fits the problem at hand? Why this and why not the other option?

and much more...

```

This is what skipping looks like.

![Skipping](https://bitbucket.org/ZelalemW/skipping-costs/raw/master/images/skipping-costs.png)

(Source: web)

Although funny, it truly depicts how some approach to master things by jumping all required skills - only to find themselves so stretched that they can't lift their feet up any more. Don't do it!

Just to give you an example here, before you become efficient in:

```

1) BI, you got to master
  - DB design
  - Query and query optimization
  - Indexing and even how indexing internally stored and works and more

2) Angular/React/Vue or JQuery or any JS librabry or framework
  - undersnad JS as language
  - Understand how JS works

3) RESTful API development say using C#,
  - Understand C# as a language
  - Understand the CLR,
  - ASP.NET /ASP.NET Core framework
  - OOD & RESTful architecture
  - Dealing with non-functional requirements (SLA, Performance, Scalability, responsiveness....)
  - Solid understanding of Data Structure and Algorithm is critical too
  - HTTP Protocol,
  - Database design and implementation and much more. Same for Java developers - the only difference is the language and framework used.


```

The skill sets listed above under each is not the complete list by no means but just to show you that at least you got to know those before declaring yourself being this or that developer.

In all cases:

***Understanding the language and runtime != Knowing the language syntax***


**One more thing...**

There are myths circulating and unfortunatly believed by many to be true. Some of those are:
```

1) Can I become developer?

Answer: What kind of question is that!!! Although, not clearly spoken of course, they are saying - they beleive they can't and they are begging to confirm that is the case so they can go home free. The bad news is: of course you can become one - assuming you have the commitment to do what's necessary.

2) O no, to become software developer, one must have solid mathematics or physics background?
Answer: Bullshit! Moving on...

3) I must have degree in Computer science or Information system or something related?

Answer: No you don't! If you have one, good, if not - study what you don't know and fill the gap. Just because you graguate doesn't mean you understood courses - even if you got A, it doesn't proof you understood it. You can score A by cheating, guessing some answers, memorizing without true understanding...

4) It takes years to become one.

Answer: Really? how do you know? If you know it yourself, then you are already a developer. If you don't know, then why are you still talking?

5) You can become developer in 3 months or less

Answer: I don't know where you heard that but that is a very short period of time for someone to grasp all that is required - assuming no background on the subject. Its good to be realstic.

6) Once I become developer, then I am all set

Answer: Not really! First of, congratulations! But you should be come a developer only if you enjoy learning and be a life long student. If not, I will kindly advice for you stay where you are.

```

I will stress this, the following image is meant to give you a general guidlines and not to be used as an absolute document. That said, its also meant to be used just for you and for you alone and NOT TO BE SHARED whatsoever or used for anything else without my consent.

![Becoming Developer](https://bitbucket.org/ZelalemW/skipping-costs/raw/master/images/BSD.jpg)


If you still struggle to get the idea, here is in simple terms:

***Tools === carbs***

***Fundamental understanding === nutrients your body needs - carb***

In the end, you need both but never ignore the importance of nutrients. It gives your body strength, energy and motivation to go further!

Let me hope you got the picture :)

**Happy coding**